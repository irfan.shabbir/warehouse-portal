import React, { useState, useEffect } from 'react'
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import PropTypes, { func } from 'prop-types';
import MuiAlert from '@material-ui/lab/Alert';
import {
    MuiThemeProvider,
    createMuiTheme,
    TablePagination,
    Dialog,
    Button,
    CardActions,
    Checkbox,
    TextField,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    FormHelperText,
    Card,
    Snackbar,
    Grid
} from '@material-ui/core';
import { addDays } from 'date-fns';
import UserModel from '../../models/UserModel'
import MaterialTable from 'material-table';
import { LensOutlined } from '@material-ui/icons';
import { Autocomplete } from '@material-ui/lab';
import { DateRangePicker } from 'react-date-range';
import { validateNumeric, validateNumericNoDecimal } from '../../common/validators';

const useStyles = makeStyles(theme => ({
    root: {},
    content: {
        padding: 0
    },
    inner: {
        minWidth: 800
    },
    statusContainer: {
        display: 'flex',
        alignItems: 'center'
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 200,
    },
    status: {
        marginRight: theme.spacing(1)
    },
    actions: {
        justifyContent: 'flex-end'
    }
}));


function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}


const Returns = (props) => {
    const [dateSelRange, setDateSelRange] = useState([
        {
            startDate: new Date(),
            endDate: addDays(new Date(), -7),
            key: 'selection'
        }
    ]);
    const [selectedBatch, setSelectedBatch] = useState();
    const [isLoad, setIsLoad] = useState(false)
    const [boolVal, setBoolVal] = useState('')
    const classes = useStyles();
    const { className, ...rest } = props;
    const [batches, setBatches] = useState([])
    const [routes, setRoutes] = useState([]);
    const [selectedRoute, setSelectedRoute] = useState();
    const [params, setParams] = useState({
        selectedRoute: null,
        from: '2020-10-21T06:00',
        to: '2020-10-21T15:00'
    }
    )
    const [openData, setOpenData] = useState({
        openSuccess: false,
        openError: false
    });
    var [data, setData] = useState([])

    const theme = createMuiTheme({
        typography: {
            fontFamily: "Nunito Sans, Roboto, sans-serif"
        }
    });

    const columns = [
        { title: 'Order Number', field: 'order_number', editable: 'never', filtering: false },
        {
            title: 'SKU',
            field: 'name',
            editable: 'never',
            filtering: false
        },
        {
            title: 'Dispatch Quantity',
            field: 'sorter_qty',
            editable: 'never',
            filtering: false
        },
        // {
        //     title: 'Quantity Req for Route',
        //     field: 'route_qtty',
        //     editable: 'never',
        //     filtering: false
        // },
        {
            title: 'Return Quantity',
            field: 'return_qty',
            editable: 'never',
            filtering: false
        },
        {
            title: 'Check',
            field: 'check',
            name: 'xyz',
            render: rowData => {
                return (
                    (<Checkbox value="true" checked={rowData.check} onChange={async (evt, val) => {
                        let newData = { sku_id: rowData.sku_id, procure_id: rowData.procure_id, is_verify: rowData.is_verify, is_sort: rowData.is_sort, name: rowData.name, sorter_qty: rowData.sorter_qty, route_qtty: rowData.route_qtty, return_qty: rowData.return_qty, check: val, act_returned_qty: rowData.return_qty, order_number: rowData.order_number }
                        const dataUpdate = [...data];
                        const index = rowData.tableData.id;
                        dataUpdate[index] = newData
                        setData([...dataUpdate])
                    }} />)
                )
            },
            filtering: false
        },
        {
            title: 'Actual Returned Quantity',
            field: 'act_returned_qty',
            editable: 'never',
            filtering: false,

            render: rowData => (rowData.check) ?
                (<TextField
                    value={rowData.return_qty}
                    disabled={true}
                />) :
                (
                    <TextField
                        disabled={false}
                        type="number"
                        value={rowData.act_returned_qty}
                        onChange={async (evt) => {
                            if (validateNumericNoDecimal(parseFloat(evt.target.value)) || !evt.target.value) {
                                console.log('rowDaata', rowData)
                                console.log('value', evt.target.value)
                                let newData = { sku_id: rowData.sku_id, is_verify: rowData.is_verify, is_sort: rowData.is_sort, procure_id: rowData.procure_id, name: rowData.name, sorter_qty: rowData.sorter_qty, route_qtty: rowData.route_qtty, return_qty: rowData.return_qty, check: rowData.check, act_returned_qty: evt.target.value, order_number: rowData.order_number }
                                const dataUpdate = [...data];
                                const index = rowData.tableData.id;
                                dataUpdate[index] = newData
                                setData([...dataUpdate])
                            }
                        }}
                    />
                )
        }
    ]

    useEffect(() => {
        let tempArr = []
        UserModel.getInstance().getBatchesManager(
            succ => {
                // console.log('succ batches', succ)
                succ.forEach((row) => {
                    // let arr = row.batch_name.split("_");
                    tempArr.push({ id: row.id, name: "batch " + row.created_at })
                })
            },
            err => {
                console.log('err batches', err)
            }
        )
        setBatches(tempArr)
    }, [])

    const handleChange = async (event, value) => {
        setIsLoad(true)
        console.log('value', value.id)
        setSelectedBatch(value);
        let tempArr = []
        UserModel.getInstance().getReturnRoute(
            value.id,
            (succ) => {
                setIsLoad(false)
                console.log("SUXXX", succ)
                succ.forEach((row) => {
                    tempArr.push({ id: row.id, name: row.name })
                })
                setRoutes(tempArr)
            },
            (err) => {
                setIsLoad(false)
                console.log(err)
            }
        )
    };

    const handleRouteChange = async (event, value) => {
        UserModel.getInstance().getReturns(
            { batch_id: selectedBatch.id, route_id: value.id },
            (succ) => {
                console.log(succ)
                setData(succ)
                setIsLoad(false)
            },
            (err) => {
                console.log(err)
                setIsLoad(false)
            }
        )
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenData({ openSuccess: false, openError: false });
    };

    const handleSubmit = () => {
        // console.log('data', data)
        let empty = false;
        let submitData = [];
        data.forEach((obj) => {
            console.log('obj', obj)
            if (obj.act_returned_qty == '') {
                empty = true;
            }
            submitData.push({
                id: obj.procure_id,
                qty: obj.act_returned_qty
            })
        })
        // console.log(submitData)

        if (empty) {
            alert('make sure you have entered all fields');
        }
        else {
            // console.log('submit data', submitData)
            UserModel.getInstance().returnConfirmation(
                { items: submitData },
                async succ => {
                    setIsLoad(false)
                    await setOpenData({ openSuccess: true });
                    console.log('succ data', succ)
                    window.location.reload();
                },
                err => {
                    setIsLoad(false)
                    setOpenData({ openError: true, openSuccess: false });
                    console.log('err', err)
                }
            )
        }
    }

    return (
        <div>
            {/* <Grid container md={6} xs={10}> */}
            <Grid container direction="row" style={{ marginLeft: 20, marginTop: 10 }}>
                <Grid direction="column" lg={3} md={4} sm={5} xs={10} style={{padding: 5}}>
                    {/* <Grid md={2}> */}
                    <Autocomplete
                        id="prodSelect"
                        options={batches}
                        getOptionLabel={option => option.name || ''}
                        renderInput={params => (
                            <TextField
                                {...params}
                                label="Batch"
                                variant="outlined"
                                margin="dense"
                                required
                                disabled
                            />
                        )}
                        fullWidth={true}
                        onChange={handleChange}
                    />
                </Grid>
                <br />
                <Grid direction="column" lg={3} md={4} sm={5} xs={10} style={{padding: 5}}>
                    <Autocomplete
                        id="prodSelect"
                        options={routes}
                        getOptionLabel={option => option.name || ''}
                        renderInput={params => (
                            <TextField
                                {...params}
                                label="Routes"
                                variant="outlined"
                                margin="dense"
                                required
                                disabled
                            />
                        )}
                        fullWidth={true}
                        onChange={handleRouteChange}
                    />
                </Grid>
            </Grid>
            {/* </Grid> */}
            {/* <div>
                <FormControl className={classes.formControl}>
                    <InputLabel id="demo-simple-select-helper-label">Batch</InputLabel>
                    <Select
                        labelId="demo-simple-select-helper-label"
                        id="demo-simple-select-helper"
                        value={selectedBatch}
                        onChange={handleChange}
                    >
                        {
                            batches.map((val) => {
                                console.log('va', val)
                                return <MenuItem value={val}>{val.batch_name}</MenuItem>
                            })
                        }
                    </Select>
                    <FormHelperText style={{ margin: 10 }}>Select a Batch Id</FormHelperText>
                </FormControl>
            </div> */}
            {/* <Grid container direction="row">
                <Grid direction="column">
                    <div style={{ marginTop: 20 }} >
                        <TextField
                            id="from"
                            label="Select From"
                            type="datetime-local"
                            defaultValue={params.from}
                            className={classes.textField}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            onChange={dateHandler}
                        />
                        <TextField
                            id="to"
                            label="Select To"
                            type="datetime-local"
                            defaultValue={params.to}
                            className={classes.textField}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            style={{ marginLeft: 40 }}
                            onChange={dateHandler}
                        />
                    </div>
                </Grid>
            </Grid> */}
            <div style={{ margin: 20 }}>
                <MaterialTable
                    title="Return Confirmation"
                    columns={columns}
                    data={data}
                    // isLoading={isLoader}
                    className={clsx(classes.root, className)}
                    // onFilterChange={filterChange}
                    isLoading={isLoad}
                    options={{
                        paging: false,
                        exportButton: true,
                        filtering: false,
                        // selection: true,
                    }}
                ></MaterialTable>
            </div>
            <Snackbar
                open={openData.openSuccess}
                autoHideDuration={3000}
                onClose={handleClose}>
                <Alert onClose={handleClose} severity="success">
                    Picker Data Successfully updated
              </Alert>
            </Snackbar>
            <Snackbar
                open={openData.openError}
                autoHideDuration={3000}
                onClose={handleClose}>
                <Alert onClose={handleClose} severity="error">
                    Error while updating Data
              </Alert>
            </Snackbar>
            <div style={{ margin: 20 }}>
                <Button color="primary" variant="contained" onClick={handleSubmit} >
                    Confirm Returns
        </Button>
            </div>
        </div>
    )
}

Returns.propTypes = {
    className: PropTypes.string
}

export default Returns
