import React, { useState, useEffect } from 'react'
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import PropTypes, { func } from 'prop-types';
import { AsyncParser, parseAsync } from 'json2csv';
import {
    MuiThemeProvider,
    createMuiTheme,
    TablePagination,
    Dialog,
    Button,
    CardActions,
    Checkbox,
    TextField,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    FormHelperText,
    Grid
} from '@material-ui/core';
import UserModel from '../../models/UserModel'
import MaterialTable from 'material-table';
import { LensOutlined } from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
    root: {},
    content: {
        padding: 0
    },
    inner: {
        minWidth: 800
    },
    statusContainer: {
        display: 'flex',
        alignItems: 'center'
    },
    status: {
        marginRight: theme.spacing(1)
    },
    actions: {
        justifyContent: 'flex-end'
    }
}));

const BatchCreate = (props) => {
    const [params, setParams] = useState({
        from: '2020-10-21T06:00',
        to: '2020-10-21T15:00',
        status: 0
    })
    const [selectedBatch, setSelectedBatch] = useState({});
    const [isLoad, setIsLoad] = useState(false)
    const [boolVal, setBoolVal] = useState('')
    const classes = useStyles();
    const { className, ...rest } = props;
    const [batches, setBatches] = useState([])
    // { id: 2, batch_name: "batch_1603439319182", created_at: "2020-10-23T07:48:39.184Z", updated_at: "2020-10-23T07:48:39.184Z", warehouse_id: 1 }
    var [data, setData] = useState([])
    const columns = [
        { title: 'SKU', field: 'sku_name', editable: 'never' },
        {
            title: 'Order Quantity',
            field: 'qtty',
            editable: 'never',
            filtering: true
        },
        {
            title: 'Warehouse Quantity',
            field: 'wh_qtty',
            editable: 'never',
            filtering: false
        },
        { title: 'Required Purchase Quantity', field: 'reqd_purchase_qtty', editable: 'never', filtering: false },
        { title: 'Procured Quantity', field: 'qty', editable: 'never', filtering: false },
        {
            title: 'Fulfilled', field: 'fulfilled', name: 'xyz', render: rowData => {
                return (
                    (rowData.is_verify) ?
                        (<Checkbox disabled={true} />)
                        :
                        (<Checkbox value="true" checked={rowData.fulfilled} onChange={async (evt, val) => {

                            let newData = { sku_name: rowData.sku_name, procurement_id: rowData.procurement_id, qtty: rowData.qtty, qty: rowData.qty, wh_qtty: rowData.wh_qtty, reqd_purchase_qtty: rowData.reqd_purchase_qtty, fulfilled: val, act_purchase: rowData.qty, total_purchase_cost: rowData.total_purchase_cost }
                            console.log('new daaata', newData)
                            const dataUpdate = [...data];
                            const index = rowData.tableData.id;
                            dataUpdate[index] = newData;
                            setData([...dataUpdate]);
                        }} />)
                )
            }
            , filtering: false
        },
        {
            title: 'Actual Procured Quantity',
            field: 'act_purchase',
            editable: 'never',
            filtering: false,

            render: rowData => (rowData.fulfilled) ? (<TextField
                value={rowData.qty}
                disabled={true}
            />) : (<TextField

                disabled={false}
                type="number"
                onChange={async (evt) => {
                    console.log('rowDaata', rowData)
                    let newData = { sku_name: rowData.sku_name, is_verify: rowData.is_verify, procurement_id: rowData.procurement_id, qtty: rowData.qtty, qty: rowData.qty, wh_qtty: rowData.wh_qtty, reqd_purchase_qtty: rowData.reqd_purchase_qtty, fulfilled: rowData.fulfilled, act_purchase: evt.target.value, total_purchase_cost: rowData.total_purchase_cost }
                    const dataUpdate = [...data];
                    const index = rowData.tableData.id;
                    dataUpdate[index] = newData
                    setData([...dataUpdate])
                }}
            />)
        },
    ]

    useEffect(() => {
        UserModel.getInstance().getBatchesManager(
            succ => {
                console.log('succ batches', succ)
                setBatches(succ)
            },
            err => {
                console.log('err batches', err)
            }
        )
    }, [])

    const handleChange = (event) => {
        setIsLoad(true)
        console.log('value', event.target.value)
        setSelectedBatch(event.target.value);
        let tempArr = [];
        UserModel.getInstance().getManagerSpecificBatch(
            event.target.value.id,
            async data => {
                console.log('procurement data', data)
                await data.forEach(obj => {
                    console.log(obj)
                    tempArr.push({
                        // obj
                        sku_name: obj.sku_name,
                        qtty: obj.order_qty,
                        wh_qtty: obj.stock,
                        reqd_purchase_qtty: obj.order_qty - obj.stock > 0 ? obj.order_qty - obj.stock : 0,
                        fulfilled: false,
                        act_purchase: obj.verify_procure_qty ? obj.verify_procure_qty : '',
                        total_purchase_cost: '',
                        procurement_id: obj.id,
                        qty: obj.procure_qty,
                        is_verify: obj.verify_procure
                    })
                })
                // console.log('temp arr', tempArr)
                setData(tempArr)
                setIsLoad(false)
            },
            err => {
                setIsLoad(false)
                console.log('err', err)
            }
        )
    };

    const handleSubmit = () => {
        let obj = [];
        let blankQty = false;
        data.forEach((row) => {
            obj.push({ id: row.procurement_id, verify_procure_qty: row.act_purchase })
            if (row.act_purchase === "") {
                blankQty = true;
            }
        })
        if (!blankQty) {
            let newObj = { items: obj }
            console.log(newObj)
            UserModel.getInstance().procurementConfirmation(
                selectedBatch.id,
                { items: obj },
                (succ) => { console.log(succ) },
                (err) => { console.log(err) }
            )
        }
        else {
            alert("Fill all quantity values")
        }
    }

    const dateHandler = async (event) => {
        console.log('evnt ', event.target.value)
        params[event.target.name] = event.target.value
        await setParams({
            ...params,
            [event.target.name]: event.target.value
        })
        console.log('params ', params)
        let from = params.from.slice(0, 10) + ' ' + params.from.slice(11, 16) + ':00';
        let to = params.to.slice(0, 10) + ' ' + params.to.slice(11, 16) + ':00';

        console.log('from', from)
        console.log('to', to)
    }

    const getBatchDownload = () => {
        UserModel.getInstance().downloadBatch(
            params,
            (succ) => {
                console.log(succ)
                parseAsync(succ).then(succ => {
                    const blob = new Blob([succ], {
                        type: 'text/csv;charset=utf-8;'
                    });
                    const blobUrl = URL.createObjectURL(blob);
                    // console.log('blob', blobUrl);
                    window.open(blobUrl);
                });
            },
            (err) => {
                console.log(err)
            }
        )
        console.log(params)
    }

    return (
        <div>
            <Grid container direction="row">
                <Grid direction="column">
                    <div style={{ marginTop: 20 }} >
                        <TextField
                            id="datetime-local"
                            label="Select From"
                            type="datetime-local"
                            name='from'
                            defaultValue={params.from}
                            className={classes.textField}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            style={{ marginLeft: 20 }}
                            onChange={dateHandler}
                        />
                        <TextField
                            id="datetime-local"
                            label="Select To"
                            type="datetime-local"
                            name='to'
                            defaultValue={params.to}
                            className={classes.textField}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            style={{ marginLeft: 20 }}
                            onChange={dateHandler}
                        />
                    </div>
                </Grid>
                <Grid direction="column">
                    <Button variant="contained" style={{ margin: 30 }} onClick={getBatchDownload}>Create Batch</Button>
                </Grid>
            </Grid>
        </div>
    )
}

BatchCreate.propTypes = {
    className: PropTypes.string
}

export default BatchCreate
