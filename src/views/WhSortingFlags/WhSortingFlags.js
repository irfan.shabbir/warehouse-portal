import React, { useState, useEffect } from 'react'
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import PropTypes, { func } from 'prop-types';
import MuiAlert from '@material-ui/lab/Alert';
import {
  MuiThemeProvider,
  createMuiTheme,
  TablePagination,
  Dialog,
  Button,
  CardActions,
  Checkbox,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  FormHelperText,
  Card,
  Snackbar
} from '@material-ui/core';
import UserModel from '../../models/UserModel'
import MaterialTable from 'material-table';
import { LensOutlined } from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 800
  },
  statusContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  formControl:{
    margin: theme.spacing(1),
    minWidth: 200,
  },
  status: {
    marginRight: theme.spacing(1)
  },
  actions: {
    justifyContent: 'flex-end'
  }
}));


function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


const WhSortingFlags = (props) => {
  const [selectedBatch, setSelectedBatch] = useState();
  const [isLoad, setIsLoad] = useState(false)
  const [boolVal, setBoolVal] = useState('')
  const classes = useStyles();
  const { className, ...rest } = props;
  const [batches, setBatches] = useState([])
  const [routes,setRoutes] = useState([]);
  const [selectedRoute,setSelectedRoute] = useState();
  const [params,setParams] = useState({
      selectedBatch:null,
      selectedRoute:null   
     }
  )
  const [openData, setOpenData] = useState({
    openSuccess: false,
    openError: false
  });
  var [data, setData] = useState([
    // {sku_id:1, sku_name: 'test1', date:'12/4/20', route_qtty: 10,  sorted_qtty:10 , fulfilled:false, act_sorted_qtty:'', sorter_name:'kumail'}
  ])

  const theme = createMuiTheme({
    typography: {
      fontFamily: "Nunito Sans, Roboto, sans-serif"
    }
  });

  const columns = [
    { title: 'SKU',
      field: 'sku_name', 
      editable: 'never', 
      filtering:false
    },
    {
      title: 'Date',
      field: 'date',
      editable: 'never',
      filtering: false
    },
    {
      title: 'order_number',
      field: 'order_number',
      editable: 'never',
      filtering: false
    },
    {
      title: 'Quantity Req for Route',
      field: 'route_qtty',
      editable: 'never',
      filtering: false
    },
    { title: 'Sorted Quantity',
      field: 'sorted_qtty', 
      editable: 'never', 
      filtering: false 
    },
    {
      title: 'Fulfilled', 
      field: 'fulfilled', 
      name: 'xyz', 
      render: rowData => {
        return (
      // (rowData.is_verify) ?
      //   (<Checkbox disabled={true} />)
      //   :
      (<Checkbox value="true" disabled={rowData.is_sort ? rowData.is_verify : true} checked={rowData.fulfilled} onChange={async (evt, val) => {
        let newData = { sku_id: rowData.sku_id, is_sort : rowData.is_sort, order_number:rowData.order_number, procurement_id:rowData.procurement_id, is_verify:rowData.is_verify, is_sort:rowData.is_sort, sku_name: rowData.sku_name, date: rowData.date, route_qtty: rowData.route_qtty, sorted_qtty: rowData.sorted_qtty, fulfilled: val, act_sorted_qtty: rowData.sorted_qtty, sorter_name: rowData.sorter_name}
        const dataUpdate = [...data];
        const index = rowData.tableData.id;
        dataUpdate[index] = newData
        setData([...dataUpdate])
        console.log('rowData',rowData)
        console.log('checked value',val)
      
        }} />)
        )
      }, 
      filtering: false
    },
    {
      title: 'Actual Sorted Quantity',
      field: 'act_sorted_qtty',
      editable: 'never',
      filtering: false,

      render: rowData => (rowData.fulfilled)?
       (<TextField
        value={rowData.sorted_qtty}
        disabled={true}
      />) : 
       (
        <TextField
        disabled={rowData.is_sort ? (rowData.is_verify ? true : false) : true}
        type="number"
        value={rowData.act_sorted_qtty}
        onChange={async (evt) => {
        console.log('rowDaata', rowData)
        console.log('value',evt.target.value)
        let newData = { sku_id: rowData.sku_id, is_sort : rowData.is_sort, is_verify:rowData.is_verify, order_number:rowData.order_number, is_sort:rowData.is_sort, procurement_id:rowData.procurement_id, sku_name: rowData.sku_name, date: rowData.date, route_qtty: rowData.route_qtty, sorted_qtty: rowData.sorted_qtty, fulfilled: rowData.fulfilled, act_sorted_qtty: evt.target.value, sorter_name: rowData.sorter_name}
        const dataUpdate = [...data];
        const index = rowData.tableData.id;
        dataUpdate[index] = newData
        setData([...dataUpdate])
        }}
        />
      )
    },
    { title: 'Sorter Id/Name', field: 'sorter_name', editable: 'never', filtering: false } 
  ]

  useEffect(() => {
    UserModel.getInstance().getBatchesManager(
      succ => {
        console.log('succ batches', succ)
        setBatches(succ)
      },
      err => {
        console.log('err batches', err)
      }
    )
  }, [])

  const handleChange = async (event) => {
    setIsLoad(true)

    setData([])
    console.log('value', event.target.value)
    console.log('name',event.target.name)
    await setParams({
      ...params,
      [event.target.name] : event.target.value
    })
    if(event.target.name == 'selectedBatch'){
     
         setParams({
          ...params,
          [event.target.name] : event.target.value,
          selectedRoute:null
        })
      
      UserModel.getInstance().getSorterRoute(
        event.target.value.id,
         async data => {
         console.log('data routes',data)
         setRoutes(data);
        },
        err => {
          alert(err)
          console.log('err',err)
        }
      )
    }
  
    // setSelectedBatch(event.target.value);
    // let tempArr = [];
    // UserModel.getInstance().getSorterConfirmation(
    //   event.target.value.id,
    //   20,
    //   async data => {
    //     console.log('sorter data',data)
    //     await data.forEach(obj => {
    //       // {sku_id:1, sku_name: 'test1', date:'12/4/20', route_qtty: 10,  sorted_qtty:10 , fulfilled:false, act_sorted_qtty:'', sorter_name:'kumail'}
                
    //             tempArr.push({
    //               // obj
    //               sku_id:obj.sku_id,
    //               sku_name: obj.name,
    //               date: obj.order_qty,
    //               route_qtty: obj.stock,
    //               reqd_purchase_qtty: obj.order_qty - obj.stock > 0 ? obj.order_qty - obj.stock : 0,
    //               fulfilled: true,
    //               act_picked: obj.order_qty - obj.stock > 0 ? obj.order_qty - obj.stock : 0,
    //               total_purchase_cost: '',
    //               procurement_id: obj.id,
    //               qty: obj.qty,
    //               is_verify: obj.is_verify
    //             })
    //           })
    //   },
    //   err => {
    //     console.log('err',err)
    //   }
    // )
  
    // UserModel.getInstance().getManagerSpecificBatch(
    //   event.target.value.id,
    //   async data => {
    //     console.log('procurement data', data)
    //     await data.forEach(obj => {
    //       console.log(obj)
    //       tempArr.push({
    //         // obj
    //         sku_name: obj.sku_name,
    //         qtty: obj.order_qty,
    //         wh_qtty: obj.stock,
    //         reqd_purchase_qtty: obj.order_qty - obj.stock > 0 ? obj.order_qty - obj.stock : 0,
    //         fulfilled: true,
    //         act_picked: obj.order_qty - obj.stock > 0 ? obj.order_qty - obj.stock : 0,
    //         total_purchase_cost: '',
    //         procurement_id: obj.id,
    //         qty: obj.qty,
    //         is_verify: obj.is_verify
    //       })
    //     })
    //     // console.log('temp arr', tempArr)
    //     setData(tempArr)
    //     setIsLoad(false)
    //   },
    //   err => {
    //     setIsLoad(false)
    //     console.log('err', err)
    //   }
    // )
  };

  const getSortingData = () => {

    console.log('params',params)
    if(params.selectedBatch && params.selectedRoute){
      // console.log('sorting')
      let tempArr = [];
    UserModel.getInstance().getSorterConfirmation(
      params.selectedBatch.id,
      params.selectedRoute.id,
      async data => {
        console.log('sorter data',data)
        await data.forEach(obj => {
          // {sku_id:1, sku_name: 'test1', date:'12/4/20', route_qtty: 10,  sorted_qtty:10 , fulfilled:false, act_sorted_qtty:'', sorter_name:'kumail'}
                
                tempArr.push({
                  // obj
                  procurement_id:obj.procure_id,
                  sku_id:obj.sku_id,
                  is_sort : obj.is_sorter,
                  sku_name: obj.name,
                  date: '',
                  route_qtty: obj.order_qty,
                  order_number : obj.order_number,
                  fulfilled: false,
                  sorted_qtty: obj.sorter_qty ? obj.sorter_qty : '',
                  act_sorted_qtty: obj.verify_sorter_qty ? obj.verify_sorter_qty : '' ,
                  is_verify: obj.verify_sorter ? true : false
                })
              })
              console.log('temp sort arr',tempArr)
              setData(tempArr)
      },
      err => {
        alert(err);
        console.log('err',err)
      }
    )      
    }
    else{
      alert('plz seleted batch and route')
    }
  }

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenData({ openSuccess: false, openError: false });
  };


  const handleSubmit = () => {
    console.log('data',data)
    let empty = false;
    let submitData = [];
    data.forEach((obj)=>{
      console.log('obj',obj)
      if(obj.act_sorted_qtty==''){
        empty = true;
      }
      submitData.push({
        id : obj.procurement_id,
        // sku_id: obj.sku_id,
        // name: obj.sku_name,
        // order_qty: obj.route_qtty,
        // sort_qty: obj.sorted_qtty,
        // sorter_verify: true,
        qty : obj.act_sorted_qtty
      })
    })

    if(empty){
      alert('make sure you have entered all field');
    }
    else{
      console.log('submit data',submitData)
      UserModel.getInstance().sorterConfirmation(
        { items: submitData },
        async succ => {
          setIsLoad(false)
          await setOpenData({ openSuccess: true });
          console.log('succ data', succ)
        },
        err => {
          // setIsLoad(false)
          setOpenData({ openError: true, openSuccess: false });
          console.log('err', err)
        }
      )
    }
    // let obj = [];
    // let blankQty = false;
    // data.forEach((row) => {
    //   obj.push({ id: row.procurement_id, qty: row.act_picked })
    //   if (row.act_picked === "") {
    //     blankQty = true;
    //   }
    // })
    // if (!blankQty) {
    //   let newObj = { items: obj }
    //   console.log(newObj)
    //   UserModel.getInstance().sorterConfirmation(
    //     selectedBatch.id,
    //     { items: obj },
    //     (succ) => { console.log(succ) },
    //     (err) => { console.log(err) }
    //   )
    // }
    // else {
    //   alert("Fill all quantity values")
    // }
  }

  return (
  
      <div>
      <FormControl className={classes.formControl}>
        <InputLabel id="demo-simple-select-helper-label">Batch</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          id="demo-simple-select-helper"
          name='selectedBatch'
          value={params.selectedBatch}
          onChange={handleChange}
        >
          {
            batches.map((val) => {
              return <MenuItem value={val}>{val.batch_name}</MenuItem>
            })
          }
        </Select>
        <FormHelperText style={{ margin: 10 }}>Select a Batch Id</FormHelperText>
      </FormControl>

      <FormControl className={classes.formControl} style={{ marginLeft: 30 }}>
        <InputLabel id="demo-simple-select-helper-label">Route</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          id="demo-simple-select-helper"
          value={params.selectedRoute}
          onChange={handleChange}
          name='selectedRoute'
        >
          {
            routes.map((val) => {
              return <MenuItem value={val}>{val.name}</MenuItem>
            })
          }
        </Select>
        <FormHelperText style={{ margin: 10 }}>Select a Route Id</FormHelperText>
      </FormControl>
      <FormControl className={classes.formControl} style={{ marginLeft: 30 }}>
      <Button color="primary" variant="contained" onClick={getSortingData} >
          Get Sorting Data
      </Button>
      </FormControl>
      <div style={{margin:20}}>
      <MaterialTable
            title="Sorting Confirmation"
            columns={columns}
            data={data}
            // isLoading={isLoader}
            className={clsx(classes.root, className)}
            // onFilterChange={filterChange}

            options={{
              paging: false,
              exportButton: true,
              filtering: false,
              // selection: true,
            }}
          ></MaterialTable>
     </div>
     <Snackbar
            open={openData.openSuccess}
            autoHideDuration={3000}
            onClose={handleClose}>
            <Alert onClose={handleClose} severity="success">
              Sorter Data Successfully updated
              </Alert>
          </Snackbar>
          <Snackbar
            open={openData.openError}
            autoHideDuration={3000}
            onClose={handleClose}>
            <Alert onClose={handleClose} severity="error">
              Error while updating Data
              </Alert>
          </Snackbar>
     <div style={{margin:20}}>
        <Button color="primary" variant="contained" onClick={handleSubmit} >
          Confirm Sorting
        </Button>
     </div>
   </div>
  )
}

WhSortingFlags.propTypes = {
  className: PropTypes.string
}

export default WhSortingFlags
