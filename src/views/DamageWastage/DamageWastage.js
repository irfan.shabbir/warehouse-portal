import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import UserModel from 'models/UserModel';
import clsx from 'clsx';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import {
    Card,
    CardHeader,
    CardContent,
    CardActions,
    CardActionArea,
    CardMedia,
    Divider,
    Grid,
    Button,
    TextField,
    Checkbox,
    Icon,
    TextareaAutosize
} from '@material-ui/core';
import Hidden from '@material-ui/core/Hidden';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import { validateNumeric } from '../../common/validators';
// import 'antd/dist/antd.css';

const GreenCheckbox = withStyles({
    root: {
        color: 'orange',
        '&$checked': {
            color: 'orange',
        },
    },
    checked: {},
})((props) => <Checkbox color="default" {...props} />);

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(4)
    },
    input: {
        display: 'none'
    },
    cardroot: {
        maxWidth: 250,
        marginTop: 30
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        flex: '1 0 auto',
        margin: theme.spacing(1),
    },
}));

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const DamageWastage = props => {
    const classes = useStyles();
    const { className, ...rest } = props;

    const [impCondition, setImpCondition] = useState(false)  // Ask before removing

    const [skuItems, setSkuItems] = useState([])
    const [selectedSkuItems, setSelectedSkuItems] = useState([{ id: null, name: '' }])
    const [damageWastageItemRows, setDamageWastageItemRows] = useState([{ name: '', quantity: '', lossType: '', reason: '' }])
    
    const lossTypes = [
        { id: '1', name: 'Damage' },
        { id: '2', name: 'Lost' },
        { id: '3', name: 'Expired' },
        { id: '4', name: 'Other' },
    ];
    const [details, setDetails] = useState('')
    // var [subTotal, setSubtotal] = useState('');

    const [openData, setOpenData] = useState({
        openSuccess: false,
        openError: false
    });
    const [params, setParams] = useState({
        dataFetchStatus: true,
        submitStatus: false,
        name: "",
    });

    const handleChange = event => {
        setParams({
            ...params,
            [event.target.name]: event.target.value
        });
    };

    const checkErrors = () => {
        var err = damageWastageItemRows && damageWastageItemRows.length > 0 && Array.isArray(damageWastageItemRows) && damageWastageItemRows.some(({ name, quantity, lossType, reason }, index) => {
            console.log(!(lossType && lossType.id == 4 && reason))
            if (
                !name || !quantity || !lossType.id || !lossType.name || !selectedSkuItems[index].id || !selectedSkuItems[index].name 
            ) {
                console.log("field(s) empty")
                return true;
            } else {
                if(quantity <= 0) {
                    console.log("Quantity cannot be less than 1")
                    return true;
                }
                if ((lossType.id == 4 && reason) || lossType.id != 4) {
                    console.log("fields OK!")
                    return false;
                } else {
                    console.log("field(s) empty")
                    return true;
                }
            }
        })
        return err;
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenData({ openSuccess: false, openError: false });
    };

    const handleSubmit = async () => {
        console.log(params.submitStatus)
        // if (!params.submitStatus) { //Commented this because it is being handled in render method //  Maaz's comment from Manual orders in sales portal
        const errors = await checkErrors() // Hamza you need to redo you error handling. It doesn not work // Maaz's comment from Manual orders in sales portal
        if (errors) {
            setOpenData({ ...openData, openError: true });
        } else {
            setParams({ ...params, submitStatus: true });
            let par = new FormData();
            let totalDamageWastageItemsList = damageWastageItemRows && damageWastageItemRows.filter(x => x.quantity > 0).map((item, index) => ({
                sku_id: selectedSkuItems[index].id,
                qty: +item.quantity,
                type: item.lossType.id == 4 ? item.reason : item.lossType.name,
                detail: details
            }))
            if (totalDamageWastageItemsList.length > 0) {
                var obj = {
                    items: totalDamageWastageItemsList
                };
                UserModel.getInstance().postDamageWastageItems(
                    obj,
                    succ => {
                        setOpenData({ ...openData, openSuccess: true });
                        setTimeout(() => {
                            window.location.reload();
                        }, 1000);
                    },
                    err => {
                        setParams({ ...params, submitStatus: false });
                        console.log(err);
                    }
                );
            }
            else {
                setParams({ ...params, submitStatus: false });
                setOpenData({ ...openData, openWarning: true });
            }
            // }
        }
    }


    const skuSearch = async (event, value) => {
        // console.log('skusearch', { event, value })
        // console.log({ event, value })
        if (value) {
            await setParams({ ...params, dataFetchStatus: false });
            await UserModel.getInstance().getSkuByName( // Ahmer bhai Please confirm this route from Shakir bhai
                { 'q': value },
                data => {
                    // console.log(data);
                    let itemsInStockArr = [];
                    data && data.length > 0 && Array.isArray(data) && data.forEach((item) => {
                        // console.log(item.is_stock, item.name)
                        if (!item.is_stock) {
                            if (item.is_deal) {
                                console.log({ item })
                                var tempItem = { ...item, name: item.name + " - DEAL" }
                                itemsInStockArr.push(tempItem)
                            } else {
                                itemsInStockArr.push(item)
                            }
                        }
                    })
                    setSkuItems([...itemsInStockArr]);
                    setParams({ ...params, dataFetchStatus: true });
                },
                err => {
                    console.log(err);
                }
            );
        }
    };

    const handleDetailsChange = event => {
        // console.log(event.target.value)
        setDetails(event.target.value);
    };

    const handleDamageWastageItemDetailsChange = (e, index) => {
        if (e.target.name === 'quantity' 
        // && validateNumeric(e.target.value)
        ) {
            // console.log(e.target.name, e.target.value, { index })
            const damageWastageItemRowsDetailArr = damageWastageItemRows;
            damageWastageItemRowsDetailArr[index][e.target.name] = e.target.value;
            setDamageWastageItemRows(damageWastageItemRowsDetailArr)
            setImpCondition(!impCondition)  // ask before removing
        } else if (e.target.name !== 'quantity') {
            // console.log(e.target.name, e.target.value, { index })
            const damageWastageItemRowsDetailArr = damageWastageItemRows;
            damageWastageItemRowsDetailArr[index][e.target.name] = e.target.value;
            setDamageWastageItemRows(damageWastageItemRowsDetailArr)
            setImpCondition(!impCondition)  // ask before removing
        }
    }

    const handleLossTypeChange = (e, index, val) => {
        // console.log(e.target.name, e.target.value, { index })
        const damageWastageItemRowsDetailArr = damageWastageItemRows;
        damageWastageItemRowsDetailArr[index]['lossType'] = val;
        setDamageWastageItemRows(damageWastageItemRowsDetailArr)
        setImpCondition(!impCondition)  // ask before removing
    }

    const damageWastageItemHandleChange = async (event, val, index) => {
        // console.log(val, index);
        if (val) {
            const skuItemsDetailArr = selectedSkuItems;
            var newObj = { id: val.id, name: val.name }
            skuItemsDetailArr[index] = newObj
            setSelectedSkuItems(skuItemsDetailArr);

            const damageWastageItemRowsDetailArr = damageWastageItemRows;
            damageWastageItemRowsDetailArr[index].name = val.name;
            damageWastageItemRowsDetailArr[index].quantity = val.quantity;
            setDamageWastageItemRows(damageWastageItemRowsDetailArr)
            setImpCondition(!impCondition)  // ask before removing
        }
    };

    const addNewDamageWastageItemRow = () => {
        // console.log('add new row')
        let rowFilled = false;
        damageWastageItemRows && damageWastageItemRows.length > 0 && Array.isArray(damageWastageItemRows) && damageWastageItemRows.forEach(({ name, quantity, lossType, reason }, index) => {
            // console.log(val)
            if (name && quantity && quantity > 0 && lossType && lossType.id && lossType.name) {
                console.log(lossType.id == 4, reason, lossType.id !== 4, lossType.id)
                if ((lossType.id == 4 && reason) || lossType.id != 4) {
                    rowFilled = true
                    return;
                } else {
                    setOpenData({ ...openData, openWarning: true });
                    rowFilled = false
                    return;
                }
            } else {
                setOpenData({ ...openData, openWarning: true });
                rowFilled = false
                return;
            }
        })
        const newDamageWastageItem = {
            name: '',
            quantity: '',
            lossType: '',
            reason: ''
        }
        const newSku = {
            id: null,
            name: '',
        }
        rowFilled && setDamageWastageItemRows([...damageWastageItemRows, newDamageWastageItem])
        rowFilled && setSelectedSkuItems([...selectedSkuItems, newSku])
        setImpCondition(!impCondition)  // ask before removing
    }

    const removeDamageWastageItem = (index) => {
        if (damageWastageItemRows.length > 1) {
            let damageWastageItemRowsDetailArr = [...damageWastageItemRows];
            damageWastageItemRowsDetailArr.splice(index, 1)
            // console.log(damageWastageItemRowsDetailArr)
            setDamageWastageItemRows([...damageWastageItemRowsDetailArr])
            let skuSelectedDetailsArr = [...selectedSkuItems];
            skuSelectedDetailsArr.splice(index, 1)
            // console.log(skuSelectedDetailsArr)
            setSelectedSkuItems([...skuSelectedDetailsArr])
            setImpCondition(!impCondition)  // ask before removing
        }
    }

    const generateDamageWastageItemsRows = (values, index) => {
        // console.log({ damageWastageItemRows, selectedSkuItems })
        // console.log({ values })
        return (
            <Grid container spacing={4}>

                <Grid item md={5} xs={12}>
                    <Autocomplete
                        id="sku"
                        // name='Sku_name'
                        options={skuItems}
                        getOptionLabel={option => option.name}
                        renderInput={params => (
                            <TextField {...params} label="SKU" variant="outlined" margin="dense" placeholder='Search SKU' />
                        )}
                        value={selectedSkuItems[index]}
                        onChange={(e, val) => damageWastageItemHandleChange(e, val, index)}
                        onInputChange={skuSearch}
                        loading
                        loadingText={
                            params.dataFetchStatus ? 'Loading' : 'No Matches'
                        }
                    />
                </Grid>

                <Grid item md={2} xs={6}>
                    <TextField
                        fullWidth
                        label="Quantity"
                        name="quantity"
                        type='number'
                        inputProps={{ min: 1 }}
                        min={1}
                        onChange={(e) => handleDamageWastageItemDetailsChange(e, index)}
                        required
                        margin='dense'
                        value={values.quantity}
                        variant="outlined"
                        placeholder="Quantity"
                    />
                </Grid>

                <Grid item md={2} xs={6}>
                    <Autocomplete
                        id="loss-types"
                        options={lossTypes}
                        getOptionLabel={option => option.name || ''}
                        value={values.lossType}
                        renderInput={params => (
                            <TextField
                                {...params}
                                margin="dense"
                                label="Loss Type"
                                variant="outlined"
                            />
                        )}
                    onChange={(e, val) => handleLossTypeChange(e, index, val)}
                    />
                </Grid>

                <Grid item md={2} xs={6}>
                    {values.lossType && values.lossType.name === 'Other' &&
                        <TextField
                            fullWidth
                            label="Reason"
                            name="reason"
                            // type='number'
                            // inputProps={{ min: 0 }}
                            // min={0}
                            onChange={(e) => handleDamageWastageItemDetailsChange(e, index)}
                            // required
                            margin='dense'
                            value={values.reason}
                            variant="outlined"
                            placeholder="Reason"
                        />
                    }
                </Grid>

                <Grid item md={1} xs={2}>
                    <Icon color="primary" style={{ fontSize: 40 }} onClick={() => removeDamageWastageItem(index)}>cancel</Icon>
                </Grid>

            </Grid>
        )
    }

    useEffect(() => {
        // console.log(skuItems)
        console.log(damageWastageItemRows)
        // console.log(selectedSkuItems)
        // console.log(details)
    })

    return (
        <div className={classes.root}>
            <Card {...rest} className={clsx(classes.root, className)}>
                <form autoComplete="off" noValidate>
                    <CardHeader title="Damage/Wastage Items" />
                    <Divider />

                    <CardContent>

                        {
                            <div id="SkuItems">
                                {/* <h5>Damage/Wastage Items</h5> */}
                                <br />
                                {damageWastageItemRows && damageWastageItemRows.length > 0 &&
                                    Array.isArray(damageWastageItemRows) &&
                                    damageWastageItemRows.map((val, index) => {
                                        return generateDamageWastageItemsRows(val, index)
                                    })
                                }
                            </div>
                        }
                        <br />
                        {/* <br /> */}

                        <CardActions style={{ display: 'flex', justifyContent: 'flex-end' }}>
                            <Icon color="primary" style={{ fontSize: 50 }} onClick={() => addNewDamageWastageItemRow()}>add_circle</Icon>
                        </CardActions>


                        <Grid item md={7} xs={12}>
                            <TextField
                                fullWidth
                                placeholder="Enter other details"
                                margin="dense"
                                name="details"
                                onChange={handleDetailsChange}
                                // required
                                multiline
                                rows={3}
                                value={details}
                                variant="outlined"
                                label="Other Details"
                            />
                        </Grid>


                        <Snackbar
                            open={openData.openWarning}
                            autoHideDuration={6000}
                            onClose={handleClose}>
                            <Alert onClose={handleClose} severity="warning">
                                Please complete above row(s) properly, before adding a new one!
                            </Alert>
                        </Snackbar>
                        <Snackbar
                            open={openData.openSuccess}
                            autoHideDuration={6000}
                            onClose={handleClose}>
                            <Alert onClose={handleClose} severity="success">
                                Damaged/Wasted SKUs submitted!
                            </Alert>
                        </Snackbar>
                        <Snackbar
                            open={openData.openError}
                            autoHideDuration={6000}
                            onClose={handleClose}>
                            <Alert onClose={handleClose} severity="error">
                                Error when making changes. Ensure all fields are filled properly!
                            </Alert>
                        </Snackbar>
                    </CardContent>
                    <Divider />
                    <CardActions>
                        <Button
                            color="primary"
                            variant="contained"
                            onClick={params.submitStatus ? null : handleSubmit}>
                            Submit
                        </Button>
                    </CardActions>
                </form>
            </Card>
        </div>
    );
};

export default DamageWastage;
