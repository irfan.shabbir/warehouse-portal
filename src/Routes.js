import React from 'react';
import { Switch, Redirect } from 'react-router-dom';
import Returns from 'views/Returns';

import { RouteWithLayout } from './components';
import { Main as MainLayout, Minimal as MinimalLayout } from './layouts';

import {
  Dashboard as DashboardView,
  Icons as IconsView,
  Account as AccountView,
  SignUp as SignUpView,
  SignIn as SignInView,
  NotFound as NotFoundView,
  Procurement,
  WarehouseConfirm,
  SetStockQuantity,
  Flags,
  ProcurementFlag,
  PickingFlag,
  SortingFlag,
  LoadingFlag,
  BatchCreate,
  DamageWastage
} from './views';

const Routes = () => {
  return (
    <Switch>
      <Redirect exact from="/" to="/dashboard" />

      <RouteWithLayout
        component={DashboardView}
        exact
        layout={MainLayout}
        path="/dashboard"
      />

      <RouteWithLayout
        component={AccountView}
        exact
        layout={MainLayout}
        path="/account"
      />

      <RouteWithLayout
        component={SignUpView}
        exact
        layout={MinimalLayout}
        path="/sign-up"
      />

      <RouteWithLayout
        component={SignInView}
        exact
        layout={MinimalLayout}
        path="/sign-in"
      />

      <RouteWithLayout
        component={BatchCreate}
        exact
        layout={MinimalLayout}
        path="/batchcreate-flag"
      />

      <RouteWithLayout
        component={Returns}
        exact
        layout={MinimalLayout}
        path="/returns-flag"
      />

      <RouteWithLayout
        component={SetStockQuantity}
        exact
        layout={MainLayout}
        path="/adhoc-procurement"
      />

      <RouteWithLayout
        component={Flags}
        exact
        layout={MainLayout}
        path="/flags"
      />
      <RouteWithLayout
        component={Procurement}
        exact
        layout={MainLayout}
        path="/procurement"
      />
      <RouteWithLayout
        component={WarehouseConfirm}
        exact
        layout={MainLayout}
        path="/procurement-flag"
      />
      {/* 
      <RouteWithLayout
        component={ProcurementFlag}
        exact
        layout={MainLayout}
        path="/procurement-flag"
      /> */}
      <RouteWithLayout
        component={PickingFlag}
        exact
        layout={MainLayout}
        path="/picking-flag"
      />
      <RouteWithLayout
        component={SortingFlag}
        exact
        layout={MainLayout}
        path="/sorting-flag"
      />
      <RouteWithLayout
        component={LoadingFlag}
        exact
        layout={MainLayout}
        path="/loading-flag"
      />
      <RouteWithLayout
        component={DamageWastage}
        exact
        layout={MainLayout}
        path="/damage-wastage"
      />
      <RouteWithLayout
        component={NotFoundView}
        exact
        layout={MinimalLayout}
        path="/not-found"
      />
      <Redirect to="/not-found" />
    </Switch>
  );
};

export default Routes;