import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Divider, Drawer } from '@material-ui/core';
import DashboardIcon from '@material-ui/icons/Dashboard';
import PeopleIcon from '@material-ui/icons/People';
import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket';
import FlagIcon from '@material-ui/icons/Flag';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import SettingsIcon from '@material-ui/icons/Settings';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import ListAltIcon from '@material-ui/icons/ListAlt';
import ShopIcon from '@material-ui/icons/Shop';
import ClearIcon from '@material-ui/icons/Clear';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import { Profile, SidebarNav, UpgradePlan } from './components';

const useStyles = makeStyles(theme => ({
  drawer: {
    width: 240,
    [theme.breakpoints.up('lg')]: {
      marginTop: 64,
      height: 'calc(100% - 64px)'
    }
  },
  root: {
    backgroundColor: theme.palette.white,
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    padding: theme.spacing(2)
  },
  divider: {
    margin: theme.spacing(2, 0)
  },
  nav: {
    marginBottom: theme.spacing(2)
  }
}));

const Sidebar = props => {
  const { open, variant, onClose, className, ...rest } = props;

  const classes = useStyles();

  const pages = [
    {
      title: 'Dashboard',
      href: '/dashboard',
      icon: <DashboardIcon />
    },
    {
      title: 'Profile',
      href: '/account',
      icon: <AccountBoxIcon />
    },
    {
      title: 'Procurement',
      href: '/procurement',
      icon: <ShopIcon />
    },
    // {
    //   title: 'Settings',
    //   href: '/s',
    //   icon: <SettingsIcon />
    // },
    // {
    //   title: 'Sku Range',
    //   href: '/sku-range',
    //   icon: <ShoppingBasketIcon />
    // },
    // {
    //   title: 'Orders',
    //   href: '/orders',
    //   icon : <ListAltIcon />
    // }\
    // {
    //   title: 'Stock Maintenance',
    //   href: '/stock-maintenance',
    //   icon : <ListAltIcon />
    // },
    {
      title: 'Flags',
      href: '/flags',
      icon: <FlagIcon />
    },
    {
      title: 'Damage/Wastage',
      href: '/damage-wastage',
      icon: <HighlightOffIcon />
    }
  ];

  return (
    <Drawer
      anchor="left"
      classes={{ paper: classes.drawer }}
      onClose={onClose}
      open={open}
      variant={variant}>
      <div {...rest} className={clsx(classes.root, className)}>
        <Profile />
        <Divider className={classes.divider} />
        <SidebarNav className={classes.nav} pages={pages} />
      </div>
    </Drawer>
  );
};

Sidebar.propTypes = {
  className: PropTypes.string,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired,
  variant: PropTypes.string.isRequired
};

export default Sidebar;
